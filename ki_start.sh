#!/bin/bash

docker-compose stop
docker-compose up -d

# Cache laravel
echo "Artisan: cache clear & build"
docker-compose exec php-kiticket php /code/artisan config:clear
docker-compose exec php-kiticket php /code/artisan config:cache

# Correr migraciones
echo "Artisan: Correr migraciones"
docker-compose exec php-kiticket php /code/artisan migrate
