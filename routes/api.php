<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/** FORM */
Route::prefix('form')->group( function() {
    Route::get('regiones', 'FormController@regiones');
    Route::get('tipos_documentos', 'FormController@tipoDocumentos');
});

Route::resource('evento', 'EventoController');
Route::get('evento/{id}/miembros', 'EventoController@miembros');

Route::resource('miembro', 'MiembroController');

Route::post('kiosko', 'KioskoController@recepcion');
