import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { store } from './store';
import { HashRouter, Route, Switch } from 'react-router-dom';
import RegisterForm from './components/register_form';
import NotFound from './components/common/404';
import Header from './components/common/header';
import Footer from './components/common/footer';
// import * as serviceWorker from './serviceWorker';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'react-toastify/dist/ReactToastify.css';


// const createStoreWithMiddleware = applyMiddleware(ReduxPromise)(createStore);

ReactDOM.render(
    <Provider store={store}>
        <HashRouter basename="/">
            <Header />
            <Switch>
                <Route exact path="/" component={RegisterForm} />
                <Route component={NotFound} />
            </Switch>
            <Footer />
        </HashRouter>
    </Provider>
    , document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
// serviceWorker.unregister();
