import { createStore, compose, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import reducers from '../reducers';

// Initial application state
const initialState = {
    registerForm: {
        regions: [],
        tipo_documento: [],
        evento: [],
        requests: 0,
        loadComunas: false,
        successRegistration: false,
        errorLoadingEvent: false
    }
};

// React browser developer tools
const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

export const store = createStore(reducers, initialState, composeEnhancers(applyMiddleware(thunk)));
