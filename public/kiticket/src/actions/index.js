import axios from 'axios';
import { toast } from "react-toastify";
import {reset} from 'redux-form';
import * as actionTypes from './actionTypes';

// TODO: ver como manejar esto cuando esté en producción
const ROOT_URL = `http://localhost:8080/api`;

/** REGIONES */

export const fetchRegions = () => dispatch =>{
    dispatch(loading());
    const url = `${ROOT_URL}/form/regiones`;
    return axios
        .get(url)
        .then(response => dispatch(fetchRegionsPromise(response)))
        .catch(error => dispatch(backEndError(error)))
};

const fetchRegionsPromise = (data) => dispatch => {
    return dispatch({
        type: actionTypes.FETCH_REGIONS,
        payload: data
    });
};


/** REGIONES */

/** TIPO DOCUMENTO */

export const fetchTipoDocumento = () => dispatch =>{
    dispatch(loading());
    const url = `${ROOT_URL}/form/tipos_documentos`;
    return axios
        .get(url)
        .then(response => dispatch(fetchTipoDocumentoPromise(response)))
        .catch(error => dispatch(backEndError(error)))
};

const fetchTipoDocumentoPromise = (data) => dispatch => {
    return dispatch({
        type: actionTypes.FETCH_TIPO_DOCUMENTOS,
        payload: data
    });
};

/** TIPO DOCUMENTO */

/** EVENTO */

// TODO: Actualmente el id está hardcoded.. hay que dejarlo dinámico según la url del evento
export const fetchEvento = () => dispatch => {
    dispatch(loading());
    const url = `${ROOT_URL}/evento/1`;
    return axios
        .get(url)
        .then(response => dispatch(fetchEventoPromise(response)))
        .catch(error => dispatch(backEndError(error)))
};

const fetchEventoPromise = (data) => dispatch => {
    return dispatch({
        type: actionTypes.FETCH_EVENTO,
        payload: data
    });
};

/** EVENTO */

/** NUEVO REGISTRO */

export const createNewRegister = (parameters) => dispatch => {
    dispatch(loading());
    const url = `${ROOT_URL}/miembro`;
    axios.defaults.headers.post['Accept'] = 'application/json';
    return axios
        .post(url,parameters)
        .then(response => dispatch(newRegisterPromise(response)))
        .catch(error => dispatch(backEndError(error)))
};

const newRegisterPromise = (response) => dispatch => {
    // limpiar form después de ingresar el nuevo registro
    console.log('responseeeee',response);
    dispatch(reset('NewRegisterForm'));
    return dispatch({
        type: actionTypes.CREATE_NEW_REGISTER
    });
};

/** NUEVO REGISTRO */

/** SET COMUNA */

export const setLoadComuna = () => dispatch => {
    return dispatch({
        type: actionTypes.SET_LOAD_COMUNA
    });
};

/** SET COMUNA */

/** MANEJANDO ERRORES AL LLAMAR A CUALQUIER REST */

const backEndError = (error) => dispatch => {
    if (error.response) {
        if(error.response.data.errors !== undefined){
            for (let property in error.response.data.errors) {
                let mensaje = error.response.data.errors[property][0];
                toast.error('🚀 '+mensaje , {
                    position: "top-center",
                    autoClose: 5000,
                    hideProgressBar: false,
                    closeOnClick: true,
                    pauseOnHover: true,
                    draggable: true,
                });
            }
        }else{
            toast.error('🚀 Error '+error.response.status , {
                position: "top-center",
                autoClose: 5000,
                hideProgressBar: false,
                closeOnClick: true,
                pauseOnHover: true,
                draggable: true,
            });
        }

    } else if (error.request) {
        toast.error('🚀 '+error.request , {
            position: "top-center",
            autoClose: 5000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true,
        });
    } else {
        toast.error('🚀 '+error.statusText , {
            position: "top-center",
            autoClose: 5000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true,
        });
    }

    return dispatch({
        type: actionTypes.FAIL_REQUEST
    })
};

/** MANEJANDO ERRORES AL LLAMAR A CUALQUIER REST */

/** SE CARGAR CUANDO SE LLAMA A UN REST */

const loading = () => dispatch => {
    return dispatch({
        type: actionTypes.LOADING
    })
};

/** SE CARGAR CUANDO SE LLAMA A UN REST */
