export const FETCH_REGIONS = 'fetch_regions';
export const FETCH_TIPO_DOCUMENTOS = 'fetch_tipo_documento';
export const FETCH_EVENTO = 'fetch_evento';
export const CREATE_NEW_REGISTER = 'create_new_register';
export const LOADING = 'loading';
export const FAIL_REQUEST = 'fail_request';
export const SET_LOAD_COMUNA = 'set_load_comuna';
