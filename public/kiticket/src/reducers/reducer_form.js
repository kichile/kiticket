import { FETCH_REGIONS, FETCH_TIPO_DOCUMENTOS, FETCH_EVENTO, FAIL_REQUEST, LOADING, CREATE_NEW_REGISTER, SET_LOAD_COMUNA } from '../actions/actionTypes';

/**
 * requests, lo que hace es aumentar en 1 por cada vez que se llame a un rest, y disminuye cada vez que el
 * rest cargue correctamente o se caiga (para esto, cada vez que se llame a un rest, hay que hacer un
 * dispatch de "LOADING"
 * */

export default function registerForm(state = [], action) {
    switch (action.type) {
        case FETCH_TIPO_DOCUMENTOS:
            return { ...state, tipo_documento: [action.payload.data.data], requests: state.requests - 1};
        case FETCH_REGIONS:
            return { ...state, regions: [action.payload.data.data], requests: state.requests - 1, loadComunas: true};
        case FETCH_EVENTO:
            return { ...state, evento: [action.payload.data.data], requests: state.requests - 1};
        case CREATE_NEW_REGISTER:
            return { ...state, requests: state.requests - 1, loadComunas: false, successRegistration: true};
        case LOADING:
            return { ...state, requests: state.requests + 1 };
        case FAIL_REQUEST:
            console.log('FAIL_REQUEST');
            return { ...state, requests: state.requests - 1, errorLoadingEvent: true };
        case SET_LOAD_COMUNA:
            return { ...state, loadComunas: true, successRegistration: false };
        default:
            return state;
    }
}

