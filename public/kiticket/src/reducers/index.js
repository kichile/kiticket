import { combineReducers } from 'redux';
import registerForm from './reducer_form';
import { reducer as formReducer } from 'redux-form';

const rootReducer = combineReducers({
    registerForm,
    form: formReducer
});

export default rootReducer;
