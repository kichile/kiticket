import React, { Component } from 'react';
import Iframe from "./common/iframe";
import { connect } from 'react-redux';
import { fetchRegions, fetchTipoDocumento, fetchEvento, createNewRegister, setLoadComuna } from "../actions/index";
import { Field, reduxForm } from 'redux-form';
import { Button, Form, FormGroup, Label, Input, Alert, Container, Row, Col, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import { ToastContainer } from "react-toastify";
import NotFound from "./common/404";
import ReactPhoneInput from 'react-phone-input-2';
import { validate as validateRut, clean, format } from 'rut.js';
import { loadComunas, successRegistration, errorLoading } from "../selectors/register_form";
import '../styles/main.css';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faSpinner, faRoute, faArrowCircleRight } from '@fortawesome/free-solid-svg-icons';

class RegisterForm extends Component{

    constructor(props) {
        super(props);
        this.state = {
            selectedOption: 'rut',
            comunasSeleccionadas: [],
            dni:'',
            id:1,
            tipoDocumento: 0,
            showMessage: false,
            modal: false
        };
        this.changeRegion = this.changeRegion.bind(this);
        this.changeTipoDocumento = this.changeTipoDocumento.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
        this.handleDni = this.handleDni.bind(this);
        this.toggleModal = this.toggleModal.bind(this);
    }

    componentDidMount() {
        /**
         * para ejecutar la llamada a los métodos que traen las regiones y los tipos de documento,
         * hay que ejecutar estos a continuación
         * */
        this.props.fetchRegions();
        this.props.fetchTipoDocumento();
        this.props.fetchEvento();
    }

    UNSAFE_componentWillMount(){
        this.props.reset();
        this.props.initialize(this.props.initialValues);
    }

    UNSAFE_componentWillReceiveProps(nextProps) {
        // TODO: esta parte esta media trucha... ver como rehacerlo
        if(nextProps.regions !== undefined && !this.props.loadComunas){
            this.setState({comunasSeleccionadas: nextProps.regions[10].comunas});
            this.setState({dni: ''});
            this.props.setLoadComuna();
        }

        if(nextProps.evento !== undefined){
            this.setState({id: nextProps.evento.id});
        }

        // mostrar mensaje de registro correcto por 5 segundos
        if(this.props.successRegistration){
            this.setState({
                showMessage: true
            });
            setTimeout(() => {
                this.setState({
                    showMessage: false
                })
            }, 5000);
        }

    }

    changeRegion(event){
        this.setState({comunasSeleccionadas: this.props.regions.filter((e) => e.id === Number.parseInt(event.target.value))[0].comunas});
    };

    changeTipoDocumento(event){
        this.setState({
            selectedOption: event.target.value
        });
        this.setState({dni: ''});
    }

    renderField(field){
        let validation = false;
        if(field.meta.touched && field.meta.error){
            validation = true;
        }
        return(
            <FormGroup>
                <Label>{field.label}</Label>
                {/*<span className="input-group-text"><FontAwesomeIcon icon={faRoute} /></span>*/}
                <Input type={field.type} name={field.name} disabled={field.disabled} invalid={validation} {...field.input}/>
                <div className="text-danger">
                    {field.meta.touched ? field.meta.error : ''}
                </div>
            </FormGroup>
        )
    }

    renderPhone(field){
        let validation = false;
        if(field.meta.touched && field.meta.error){
            validation = true;
        }
        return(
            <FormGroup>
                <Label>{field.label}</Label>
                <ReactPhoneInput
                    style={{width: '100%'}}
                    placeholder="Teléfono"
                    defaultCountry={'cl'}
                    invalid={validation}
                    masks={{'cl': '+.. (.) .... ....', 'at': '+...........'}}
                    {...field.input}
                />
                <div className="text-danger">
                    {field.meta.touched ? field.meta.error : ''}
                </div>
            </FormGroup>
        )
    }

    renderSelect(field){
        return(
            <FormGroup>
                <Label for="exampleSelect">{field.label}</Label>
                <Input type={field.type} name={field.name} onChange={field.onChange} disabled={field.disabled} {...field.input}>
                    {
                        field.values.map(value => {
                            return (
                                <option key={value.id} value={value.id}>
                                    {value.nombre}
                                </option>
                            );
                         })
                    }
                </Input>
            </FormGroup>
        )
    }

    renderFieldRadioButton(field){
        let validation = false;
        if(field.meta.touched && field.meta.error){
            validation = true;
        }
        // seteamos valor formateado, en el caso que sea rut
        field.input.value = field.valorDni;
        return(
            <FormGroup>
                <Input type={field.type} name={field.name} onChange={field.onChange} disabled={field.disabled} invalid={validation} {...field.input}/>
                <div className="text-danger">
                    {field.meta.touched ? field.meta.error : ''}
                </div>
            </FormGroup>
        )
    }

    // renderRadioButton(field){
    //     return(
    //         <div className="radio-dni">
    //             <Input {...field.input} type={field.type} checked={field.checked} />
    //             <label>{field.label}</label>
    //         </div>
    //     )
    // }

    handleDni(event){
        if(this.state.selectedOption === 'rut'){
            this.setState({dni: format(event.target.value)});
        }else {
            this.setState({dni: event.target.value});
        }
    }

    toggleModal() {
        this.setState(prevState => ({
            modal: !prevState.modal
        }));
    }

    onSubmit(values){

        // TODO: esto hay que manejarlo de otra forma (está muy rasca)
        if(this.state.selectedOption === 'rut'){
            values.dni = clean(values.dni);
            values.dni = values.dni.substr(0,values.dni.length-1)+'-'+values.dni.substr(values.dni.length-1,values.dni.length);
        }

        let dniState = this.state.selectedOption;

        this.props.tipo_documento.forEach(function (value) {
            if(value.nombre.toLowerCase() === dniState.toLowerCase()){
                values.tipo_documento_id = value.id;
            }
        });

        let eventoId = this.props.evento.id;

        let parameters = {
            evento_id: eventoId,
            tipo_documento_id: values.tipo_documento_id,
            dni: values.dni,
            nombre: values.nombre,
            apellido_paterno: values.apellido_paterno,
            apellido_materno: values.apellido_materno,
            cargo: values.cargo,
            empresa: values.empresa,
            comuna_id: parseInt(values.comuna),
            celular: parseInt(values.celular),
            email: values.email,
            direccion: values.direccion
        };

        this.props.createNewRegister(parameters);

    }

    render(){
        const { handleSubmit, regions, tipo_documento, requests, evento } = this.props;
        const showSuccesRegister = this.state.showMessage;

        // si hay un error en algun request, mostrar página de error
        const errorLoading = this.props.errorLoading;

        console.log('errorLoading',errorLoading);

        let loadingData = true;
        // requests es igual a cero cuando todos las llamadas a los rest terminan
        if(requests === 0 && this.props.regions !== undefined){
            loadingData = false;
        }

        return(
            loadingData && !errorLoading ?
                <Container>
                    <ToastContainer />
                    <div className="spinner-form">
                        <FontAwesomeIcon icon={faSpinner} size="6x" spin/>
                    </div>
                </Container>
                :
                errorLoading ?
                    <NotFound />
                :
                    <Container>
                        <ToastContainer />
                        {
                            showSuccesRegister ?
                                <Alert color="success" className="mrbt0">
                                    Registro completado exitosamente!!
                                </Alert>
                            :
                                ''
                        }

                        <Form onSubmit={handleSubmit(this.onSubmit)} className="container-form-register" >
                            <Container>
                                <Row>
                                    <Col lg="4"><Field label="Nombre" type="text" name="nombre" disabled={loadingData} component={this.renderField}/></Col>
                                    <Col lg="4"><Field label="Apellido Paterno" type="text" disabled={loadingData} name="apellido_paterno" component={this.renderField}/></Col>
                                    <Col lg="4"> <Field label="Apellido Materno" type="text" disabled={loadingData} name="apellido_materno" component={this.renderField}/></Col>
                                </Row>
                                <Row>
                                    <Col lg="4">
                                        {/*<Field name="radio_dni" component={this.renderRadioButton} onChange={this.changeTipoDocumento} type="radio" checked={true} value="rut" label="Rut"/>*/}
                                        {/*<Field name="radio_dni" component={this.renderRadioButton} onChange={this.changeTipoDocumento} type="radio" value="pasaporte" label="Pasaporte"/>*/}
                                        {/*checked={this.state.selectedOption === 'rut'}*/}
                                        {/*checked={this.state.selectedOption === 'pasaporte'}*/}

                                        {/*TODO: esta parte hay que automatizarla (con los datos que vengan desde el rest de tipo_documento */}
                                        {
                                            tipo_documento.length > 0 ?
                                                <div className="radio-button-dni">
                                                    <div className="radio-dni">
                                                        <label>
                                                            <Field type="radio" id="radio_dni" component="input" name="radio_dni" value='rut' onChange={this.changeTipoDocumento} disabled={loadingData}/>
                                                            {' '} Rut
                                                        </label>
                                                    </div>
                                                    <div className="radio-dni">
                                                        <label>
                                                            <Field type="radio" id="radio_dni" component="input" name="radio_dni" value='pasaporte'  onChange={this.changeTipoDocumento} disabled={loadingData}/>
                                                            {' '} Pasaporte
                                                        </label>
                                                    </div>
                                                </div>
                                                : ''
                                        }
                                        {/*<Field type="text" name="dni" label="Dni" valorDni={this.state.dni} onChange={this.handleDni} disabled={loadingData} component={this.renderFieldRadioButton}/>*/}
                                        <Field type="text" name="dni" label="Dni" valorDni={this.state.dni} onChange={this.handleDni} disabled={loadingData} component={this.renderFieldRadioButton}/>
                                    </Col>
                                    <Col lg="4"><Field label="Cargo" type="text" name="cargo" disabled={loadingData} component={this.renderField}/></Col>
                                    <Col lg="4"><Field label="Empresa" type="text" name="empresa" disabled={loadingData} component={this.renderField}/></Col>
                                </Row>
                                <Row>
                                    <Col lg="5"><Field label="Dirección" type="text" name="direccion" disabled={loadingData} component={this.renderField}/></Col>
                                    <Col lg="4"><Field label="Región" type="select" name="region" values={regions} onChange={this.changeRegion} disabled={loadingData} component={this.renderSelect}/></Col>
                                    <Col lg="3"><Field label="Comuna" type="select" name="comuna" values={this.state.comunasSeleccionadas} disabled={loadingData} component={this.renderSelect}/></Col>
                                </Row>
                                <Row>
                                    <Col lg="4">
                                        <Field label="Celular (9 dígitos)" type="number" name="celular" disabled={loadingData} component={this.renderField}/>
                                        {/*<Field label="Celular" type="number" name="celular" disabled={loadingData} component={this.renderPhone}/>*/}
                                    </Col>
                                    <Col lg="4"><Field label="Email" type="email" name="email" disabled={loadingData} component={this.renderField}/></Col>
                                </Row>

                                <Row>
                                    <Col>
                                        <div className="float-right">
                                            {/*<Spinner color="primary" />*/}
                                            <Button color="info" disabled={loadingData} type="submit">Enviar <FontAwesomeIcon icon={faArrowCircleRight} /></Button>
                                        </div>
                                    </Col>
                                </Row>

                            </Container>
                        </Form>

                        <Button color="success" className="btnComoLlegar" onClick={this.toggleModal}>Como llegar <FontAwesomeIcon icon={faRoute} /></Button>
                        <Modal isOpen={this.state.modal} toggle={this.toggleModal} size="lg">
                            <ModalHeader toggle={this.toggleModal}>Como llegar</ModalHeader>
                            <ModalBody>
                                <Iframe src={evento.maps} />
                            </ModalBody>
                            <ModalFooter>
                                {/*<Button color="primary" onClick={this.toggleModal}>Do Something</Button>{' '}*/}
                                <Button color="secondary" onClick={this.toggleModal}>Cancel</Button>
                            </ModalFooter>
                        </Modal>

                    </Container>

        );

    }
}

function validate(values, props){

    const emailRex = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    const errors = {};

    // validar los inputs.. agregar todas las validaciones necesarias
    /** validaciones DNI*/

    if(!values.dni){
        errors.dni = 'Agregar Rut o Pasaporte!';
    }else if(values.radio_dni === 'rut'){
        if(!validateRut(format(values.dni))){
            errors.dni = 'Rut Inválido!';
        }
    }

    /** validaciones nombre*/
    if(!values.nombre){
        errors.nombre = 'Agregar Nombre!';
    }

    /** validaciones apellido paterno*/
    if(!values.apellido_paterno){
        errors.apellido_paterno = 'Agregar Apellido Paterno!';
    }

    /** validaciones apellido materno*/
    if(!values.apellido_materno){
        errors.apellido_materno = 'Agregar Apellido Materno!';
    }

    /** validaciones cargo*/
    if(!values.cargo){
        errors.cargo = 'Agregar Cargo!';
    }

    /** validaciones empresa*/
    if(!values.empresa){
        errors.empresa = 'Agregar Empresa!';
    }

    /** validaciones direccion*/
    if(!values.direccion){
        errors.direccion = 'Agregar Dirección!';
    }

    /** validaciones celular*/
    if(!values.celular){
        errors.celular = 'Agregar Celular!';
    }else if(values.celular.length !== 9){
        errors.celular = 'Formato no es correcto (debe tener 9 dígitos)';
    }

    /** validaciones email*/
    if(!values.email){
        errors.email = 'Agregar Email!';
    }else if(!emailRex.test(values.email)){
        errors.email = 'Error en el formato del Email!';
    }

    // si errors tiene alguna propiedad, redux form asume que el form es invalido
    return errors;
}

const mapDispatchToProps = dispatch => ({
    fetchRegions: () => dispatch(fetchRegions()),
    fetchTipoDocumento: () => dispatch(fetchTipoDocumento()),
    fetchEvento: () => dispatch(fetchEvento()),
    createNewRegister: (parameters) => dispatch(createNewRegister(parameters)),
    setLoadComuna: () => dispatch(setLoadComuna())
});

// TODO: el initialValues no me convence, veré de que forma arreglarlo
function mapStateToProps(state) {
    console.log('state',state);
    return {
        regions: state.registerForm.regions[0],
        tipo_documento:  state.registerForm.tipo_documento[0],
        evento: state.registerForm.evento[0],
        requests: state.registerForm.requests,
        loadComunas: loadComunas(state),
        successRegistration: successRegistration(state),
        errorLoading: errorLoading(state),
        initialValues: {
            radio_dni:'rut',
            dni:'',
            region: state.registerForm.regions.length > 0 ? state.registerForm.regions[0][10].id : 11,
            comuna: state.registerForm.regions.length > 0 ? state.registerForm.regions[0][0].comunas[0].id : 267,
        }
    }
}

RegisterForm = connect(mapStateToProps,mapDispatchToProps)(RegisterForm);

export default reduxForm({
    validate,
    form: 'NewRegisterForm',
    enableReinitialize: true,
    keepDirtyOnReinitialize: true
})(RegisterForm);
