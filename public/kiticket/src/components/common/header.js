import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Container, Row, Col } from 'reactstrap';
import {ToastContainer} from "react-toastify";

class Header extends Component {

    render() {
        let banner='';
        if(this.props.evento !== undefined){
            banner = this.props.evento.banner;
        }

        return (
            <Container className="mrbt25">
                <ToastContainer />
                <Row>
                    <Col>
                        <img src={banner} width="100%"/>
                    </Col>
                </Row>
            </Container>
        );
    }
}

function mapStateToProps(state) {
    return {
        evento: state.registerForm.evento[0]
    }
}

export default connect(mapStateToProps)(Header);
