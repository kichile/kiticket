import React, { Component } from 'react';
import { Container, Row, Col } from 'reactstrap';

export default class Footer extends Component {
    render() {
        return (
            <Container>
                <Row>
                    <Col>
                        <div className="footer-ki">
                            KiChile 2019
                        </div>
                    </Col>
                </Row>
            </Container>
        );
    }
}
