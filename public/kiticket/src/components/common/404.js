import React, { Component } from 'react';
import Container from 'reactstrap/es/Container';
import '../../styles/not-found.css';

export default class NotFound extends Component {
    render() {
        return (
            <Container >
                <div className="background">
                    <h1>Whoops!</h1>
                    <p>Algo salió mal...</p>
                </div>
            </Container>
        );
    }
}
