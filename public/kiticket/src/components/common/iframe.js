import React, { Component } from 'react';

class Iframe extends Component {

    iframe() {
        return {
            __html: this.props.src
        }
    }

    render() {
        return (
            <div dangerouslySetInnerHTML={ this.iframe() } />
        );
    }
}

export default Iframe;
