<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Miembro extends Model
{
    const FIELD_TIPO_DOCUMENTO_ID = "tipo_documento_id";
    const FIELD_DNI = "dni";
    const FIELD_NOMBRE = "nombre";
    const FIELD_APELLIDO_PATERNO = "apellido_paterno";
    const FIELD_APELLIDO_MATERNO = "apellido_materno";
    const FIELD_CARGO = "cargo";
    const FIELD_EMPRESA = "empresa";
    const FIELD_DIRECCION = "direccion";
    const FIELD_COMUNA_ID = "comuna_id";
    const FIELD_CELULAR = "celular";
    const FIELD_EMAIL = "email";

    const FIELDS = [
        self::FIELD_TIPO_DOCUMENTO_ID,
        self::FIELD_DNI,
        self::FIELD_NOMBRE,
        self::FIELD_APELLIDO_PATERNO,
        self::FIELD_APELLIDO_MATERNO,
        self::FIELD_CARGO,
        self::FIELD_EMPRESA,
        self::FIELD_DIRECCION,
        self::FIELD_COMUNA_ID,
        self::FIELD_CELULAR,
        self::FIELD_EMAIL,
    ];

    /**
     * @var string
     */
    protected $table = 'miembros';

    protected $fillable = self::FIELDS;

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function eventos()
    {
        return $this->belongsToMany(Evento::class);
    }
}
