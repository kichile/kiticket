<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Evento extends Model
{
    const FIELD_ID = "id";

    /**
     * @var string
     */
    protected $table = 'eventos';

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function miembros()
    {
        return $this->belongsToMany(Miembro::class);
    }
}



