<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EventoMiembro extends Model
{
    const FIELD_EVENTO_ID = 'evento_id';
    const FIELD_MIEMBRO_ID = 'miembro_id';

    const FIELDS = [
        self::FIELD_EVENTO_ID,
        self::FIELD_MIEMBRO_ID
    ];

    /**
     * @var string
     */
    protected $table = 'evento_miembro';

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var array
     */
    protected $fillable = self::FIELDS;
}
