<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Region extends Model
{
    /**
     * @var string
     */
    protected $table = 'regiones';
    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasManyThrough
     */
    public function comunas()
    {
        return $this->hasManyThrough(Comuna::class, Provincia::class)->orderBy('nombre');
    }
}
