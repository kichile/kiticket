<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TipoDocumento extends Model
{
    /**
     * @var string
     */
    protected $table = 'tipos_documentos';

    /**
     * @var bool
     */
    public $timestamps = false;
}
