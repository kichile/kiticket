<?php

namespace App\Http\Controllers;

use App\Models\Comuna;
use App\Models\Region;
use App\Models\TipoDocumento;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class FormController extends BaseController
{
    /**
     * @return \Illuminate\Http\Response
     */
    public function regiones()
    {
        return $this->sendResponse(Region::orderBy('id')->get()->load('comunas')->toArray());
    }

    /**
     * @return \Illuminate\Http\Response
     */
    public function tipoDocumentos()
    {
        return $this->sendResponse(TipoDocumento::all()->toArray());
    }
}
