<?php

namespace App\Http\Controllers;

use App\Models\Evento;
use App\Models\EventoMiembro;
use App\Models\Miembro;
use Illuminate\Http\Request;
use App\Http\Requests\StoreMiembro;
use Illuminate\Support\Arr;

class MiembroController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * @param StoreMiembro $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreMiembro $request)
    {
        $validated = array_map('mb_strtoupper', $request->validated());
        $miembroData = Arr::only($validated, Miembro::FIELDS);

        $miembro = Miembro::firstOrCreate($miembroData);

        EventoMiembro::firstOrCreate([
            EventoMiembro::FIELD_EVENTO_ID => $validated[EventoMiembro::FIELD_EVENTO_ID],
            EventoMiembro::FIELD_MIEMBRO_ID => $miembro->id
        ]);

        $evento = Evento::find($validated[EventoMiembro::FIELD_EVENTO_ID]);

        return $this->sendResponse([
            'miembro' => $miembro->toArray(),
            'evento' => $evento->toArray()
        ], '¡El registro se ha completado con éxito!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
