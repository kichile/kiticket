<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;

class KioskoController extends BaseController
{
    public function recepcion(Request $request)
    {
        if ($request->has('data'))
        {
            $this->sendErrorResponse('No estan los atributos necesarios para procesar la información');
        }

        $data = json_decode($request->get('data'), true);

        $batch_id = Carbon::now()->timestamp . '-' . gethostname();

        foreach($data as &$record)
        {
            $record['batch_id'] = $batch_id;
        }

        try {
            \DB::table('kiosko_miembros_recibidos')->insert($data);
        } catch(\Exception $e) {
            $this->sendErrorResponse('Ocurrió un error al almacenar la información');
        }

        return $this->sendResponse([
            'batch_id' => $batch_id,
            'registros_insertados' => \DB::table('kiosko_miembros_recibidos')->where('batch_id', $batch_id)->count()
        ], '¡Los datos se han almacenado exitosamente!');
    }
}
