<?php

namespace App\Http\Requests;

use App\Models\EventoMiembro;
use App\Models\Miembro;
use Illuminate\Foundation\Http\FormRequest;

class StoreMiembro extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            EventoMiembro::FIELD_EVENTO_ID => 'required|numeric|exists:eventos,id',
            Miembro::FIELD_TIPO_DOCUMENTO_ID => 'required|numeric|exists:tipos_documentos,id',
            Miembro::FIELD_DNI => 'required|unique:miembros,dni',
            Miembro::FIELD_NOMBRE => 'required|alpha',
            Miembro::FIELD_APELLIDO_PATERNO => 'required|alpha',
            Miembro::FIELD_APELLIDO_MATERNO => 'required|alpha',
            Miembro::FIELD_CARGO => 'required',
            Miembro::FIELD_EMPRESA => 'required',
            Miembro::FIELD_DIRECCION => 'nullable',
            Miembro::FIELD_COMUNA_ID => 'required|numeric|exists:comunas,id',
            Miembro::FIELD_CELULAR => 'required|numeric',
            Miembro::FIELD_EMAIL => 'required|email'
        ];
    }

    /**
     * @return array
     */
    public function attributes()
    {
        return [
            EventoMiembro::FIELD_EVENTO_ID => 'evento',
            Miembro::FIELD_TIPO_DOCUMENTO_ID => 'tipo de documento',
            Miembro::FIELD_DNI => 'DNI',
            Miembro::FIELD_NOMBRE => 'nombre',
            Miembro::FIELD_APELLIDO_PATERNO => 'apellido paterno',
            Miembro::FIELD_APELLIDO_MATERNO => 'apellido materno',
            Miembro::FIELD_CARGO => 'cargo',
            Miembro::FIELD_EMPRESA => 'empresa',
            Miembro::FIELD_DIRECCION => 'dirección',
            Miembro::FIELD_COMUNA_ID => 'comuna',
            Miembro::FIELD_CELULAR => 'celular',
            Miembro::FIELD_EMAIL => 'correo electrónico'
        ];
    }

    /**
     * @return array
     */
    public function messages()
    {
        return [
            # REQUIRED
            EventoMiembro::FIELD_EVENTO_ID . '.required' => 'El :attribute es requerido',
            Miembro::FIELD_TIPO_DOCUMENTO_ID . '.required' => 'El :attribute es requerido',
            Miembro::FIELD_DNI . '.required' => 'El :attribute es requerido',
            Miembro::FIELD_NOMBRE . '.required' => 'El :attribute es requerido',
            Miembro::FIELD_APELLIDO_PATERNO . '.required' => 'El :attribute es requerido',
            Miembro::FIELD_APELLIDO_MATERNO . '.required' => 'El :attribute es requerido',
            Miembro::FIELD_CARGO . '.required' => 'El :attribute es requerido',
            Miembro::FIELD_EMPRESA . '.required' => 'La :attribute es requerido',
            Miembro::FIELD_COMUNA_ID . '.required' => 'La :attribute es requerida',
            Miembro::FIELD_CELULAR . '.required' => 'El :attribute es requerido',
            Miembro::FIELD_EMAIL . '.required' => 'El :attribute es requerido',

            # EXISTS
            EventoMiembro::FIELD_EVENTO_ID . '.exists' => 'El :attribute ingresado no existe',
            Miembro::FIELD_TIPO_DOCUMENTO_ID . '.exists' => 'El :attribute ingresado no existe',
            Miembro::FIELD_COMUNA_ID . '.exists' => 'La :attribute ingresado no existe',

            # UNIQUE
            Miembro::FIELD_DNI . '.unique' => 'El :attribute ingresado ya existe, se encuentra registrado en nuestra base',

            # ALPHA
            Miembro::FIELD_NOMBRE . '.alpha' => 'El :attribute ingresado debe contener solo caracteres',
            Miembro::FIELD_APELLIDO_PATERNO . '.alpha' => 'El :attribute ingresado debe contener solo caracteres',
            Miembro::FIELD_APELLIDO_MATERNO . '.alpha' => 'El :attribute ingresado debe contener solo caracteres',
        ];
    }
}
