<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Init extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        # Regiones
        Schema::create('regiones', function(Blueprint $table) {
            $table->increments('id');
            $table->string('nombre', 100);
            $table->string('nombre_original', 5);
        });

        # Provincias
        Schema::create('provincias', function(Blueprint $table) {
            $table->increments('id');
            $table->string('nombre', 100);
            $table->integer('region_id')->unsigned();

            $table->foreign('region_id')->references('id')->on('regiones');
        });

        # Comunas
        Schema::create('comunas', function(Blueprint $table) {
            $table->increments('id');
            $table->string('nombre', 100);
            $table->integer('provincia_id')->unsigned();

            $table->foreign('provincia_id')->references('id')->on('provincias');
        });

        # DNI o RUT
        Schema::create('tipos_documentos', function(Blueprint $table) {
            $table->increments('id');
            $table->string('nombre');
        });

        # Miembros
        Schema::create('miembros', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('tipo_documento_id')->unsigned();
            $table->string('dni', 50);
            $table->string('nombre', 100);
            $table->string('apellido_paterno', 100);
            $table->string('apellido_materno', 100);
            $table->string('cargo', 100);
            $table->string('empresa', 100);
            $table->string('direccion', 100)->nullable();
            $table->integer('comuna_id')->unsigned();
            $table->integer('celular');
            $table->string('email', 50);
            $table->timestamps();

            $table->foreign('tipo_documento_id')->references('id')->on('tipos_documentos');
            $table->foreign('comuna_id')->references('id')->on('comunas');
        });

        # Clientes
        Schema::create('clientes', function(Blueprint $table) {
            $table->increments('id');
            $table->string('rut', 15);
            $table->string('dv', 1);
            $table->string('nombre', 50);
            $table->string('direccion', 100)->nullable();
            $table->integer('comuna_id')->unsigned();
            $table->integer('celular');
            $table->string('email', 50);
            $table->timestamps();

            $table->foreign('comuna_id')->references('id')->on('comunas');
        });

        # Eventos
        Schema::create('eventos', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('cliente_id')->unsigned();
            $table->string('nombre', 50);
            $table->date('fecha');
            $table->string('direccion',100);
            $table->integer('comuna_id')->unsigned();
            $table->timestamps();

            $table->foreign('cliente_id')->references('id')->on('clientes');
            $table->foreign('comuna_id')->references('id')->on('comunas');
        });

        # Un evento puede tener muchas personas y muchas personas a muchos eventos
        Schema::create('evento_miembro', function(Blueprint $table) {
            $table->integer('evento_id')->unsigned();
            $table->integer('miembro_id')->unsigned();

            $table->foreign('evento_id')->references('id')->on('eventos');
            $table->foreign('miembro_id')->references('id')->on('miembros');
        });

        /** DATA */
        # Comunas, provincias y regiones
        DB::unprepared(file_get_contents(dirname(__FILE__) . '/2019_08_20_033907_init_comunas.sql'));

        # Tipos de documento
        DB::table('tipos_documentos')->insert([
            ['nombre' => 'RUT'],
            ['nombre' => 'PASAPORTE']
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('evento_miembro');
        Schema::dropIfExists('eventos');
        Schema::dropIfExists('miembros');
        Schema::dropIfExists('tipos_documentos');
        Schema::dropIfExists('clientes');
        Schema::dropIfExists('comunas');
        Schema::dropIfExists('provincias');
        Schema::dropIfExists('regiones');
    }
}
