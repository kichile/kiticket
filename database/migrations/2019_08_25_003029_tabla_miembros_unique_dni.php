<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TablaMiembrosUniqueDni extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('miembros', function (Blueprint $table) {
            $table->unique('dni');
        });

        Schema::table('evento_miembro', function(Blueprint $table) {
            $table->unique(['evento_id', 'miembro_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('miembros', function (Blueprint $table) {
            $table->dropUnique('dni');
        });

        Schema::table('evento_miembro', function(Blueprint $table) {
            $table->dropUnique(['evento_id', 'miembro_id']);
        });
    }
}
