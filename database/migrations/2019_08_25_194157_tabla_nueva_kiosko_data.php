<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TablaNuevaKioskoData extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kiosko_miembros_recibidos', function(Blueprint $table) {
            $table->string('batch_id');
            $table->integer('id')->nullable();
            $table->integer('evento_id');
            $table->integer('tipo_documento_id')->nullable();
            $table->string('dni', 50)->nullable();
            $table->string('nombre', 100)->nullable();
            $table->string('apellido_paterno', 100)->nullable();
            $table->string('apellido_materno', 100)->nullable();
            $table->string('cargo' ,100)->nullable();
            $table->string('empresa', 100)->nullable();
            $table->integer('celular')->nullable();
            $table->string('email', 50)->nullable();
            $table->boolean('registrado_en_evento')->default(0);
            $table->boolean('hizo_checkin')->default(0);
            $table->integer('cantidad_ingresos')->default(0);
            $table->boolean('es_staff')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('kiosko_miembros_recibidos');
    }
}
