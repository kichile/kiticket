<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TablaEventosNuevosCampos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('eventos', function(Blueprint $table) {
            $table->text('maps')->nullable()->after('comuna_id');
            $table->text('banner')->nullable()->after('comuna_id');
        });

        DB::unprepared(file_get_contents(dirname(__FILE__) . '/2019_08_24_212609_tabla_eventos_nuevos_campos.sql'));
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('eventos', function(Blueprint $table) {
            $table->dropColumn('maps');
            $table->dropColumn('banner');
        });
    }
}
