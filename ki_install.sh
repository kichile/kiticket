#!/bin/bash

# Copiar .env
cp .env.example .env

# Eliminar dockers si es que existen
echo "Docker: Eliminando dockers existentes"
docker-compose rm -s -v -f php-kiticket app-kiticket db-kiticket

# Generar containers
echo "Docker: Generar nuevos containers para app ki-checkin"

# Levantar servicios docker
docker-compose up -d app-kiticket

# Instalar dependencias php
docker-compose exec php-kiticket composer install

# restart
docker-compose stop
docker-compose up -d

# Cache laravel
echo "Artisan: cache clear & build"
docker-compose exec php-kiticket php /code/artisan config:clear
docker-compose exec php-kiticket php /code/artisan config:cache

# Correr migraciones
echo "Artisan: Correr migraciones"
docker-compose exec php-kiticket php /code/artisan migrate
